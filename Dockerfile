# Сборка --------------------------------------- (multi-stage build)

# В качестве базового образа для сборки используем уже кем-то подготовленный образ с opencv
FROM jjanzic/docker-python3-opencv:latest as build

# Рабочая директория для сборки GoogleTest
WORKDIR /gtest_build

RUN apt-get update && \
    apt-get install -y \
      libboost-dev libboost-program-options-dev \
      libgtest-dev \
      cmake \
      # здесь также может быть список дополнительных библиотек, используемых проектом

# Cборка GoogleTest
RUN cmake -DCMAKE_BUILD_TYPE=Release /usr/src/gtest && \
    cmake --build . && \
    mv lib*.a /usr/lib

# Скопируем директорию /src в контейнер
ADD ./src /app/src

# Рабочая директория для сборки проекта
WORKDIR /app/build

# Сборка + тестирование
RUN cmake ../src && \
    cmake --build . && \
    CTEST_OUTPUT_ON_FAILURE=TRUE cmake --build . --target test

# Запуск ---------------------------------------

# В качестве базового образа, например, используем ubuntu:latest
FROM ubuntu:latest

# Рабочая директория нашего приложения
WORKDIR /app

# Скопируем приложение со сборочного контейнера в рабочую директорию
COPY --from=build /app/build/processing_app .

# Точка входа
ENTRYPOINT ["./processing_app"]
